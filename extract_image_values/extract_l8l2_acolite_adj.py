import numpy as np
from netCDF4 import Dataset
import configparser
import pandas as pd
import glob, os, math
import gdal, osr, ogr

'''
Important:
 data that read by gdal from NetCDF format is not the same as HDF5
 so, I make this changes:
 lon = xDataset.ReadAsArray(xoff=i1, yoff=self.RasterX-i0-1, xsize=1, ysize=1)[0][0]
 lat = yDataset.ReadAsArray(xoff=i1, yoff=self.RasterX-i0-1, xsize=1, ysize=1)[0][0]

 values_v = gdal.Open(sds[indexs[0]][0]).ReadAsArray(xoff= pixel-2,yoff=self.RasterX-line-2-1,xsize=5,ysize=5)

'''


class workspace():

    def __init__(self, config):
        self.l2dir = config['root']['l2dir']
        self.productid_station_f = config['root']['productid_station_f']

        sds = ['median', 'mean', 'std', 'max', 'min']
        self.wavelength = ['443', '482', '561', '655', '865', '1609', '2201']

        self.bands = ['Rrs_%s' % (w) for w in self.wavelength]
        self.bands.extend(['rhot_%s' % (w) for w in self.wavelength])
        self.bands.extend(['rhom_%s' % (w) for w in self.wavelength])

        self.bands.extend(['tau_865', 'angstrom', 'solz', 'senz', 'relaz', 'pressure', 'water_vapor', 'l2_flags'])

        self.columns = ['SiteName', 'l2file', 'algorithm', 'line', 'pixl']
        self.columns.extend(['Rrs_%s_%s' % (w, sd) for w in self.wavelength for sd in sds])
        self.columns.extend(['rhot_%s_%s' % (w, sd) for w in self.wavelength for sd in sds])
        self.columns.extend(['rhom_%s_%s' % (w, sd) for w in self.wavelength for sd in sds])
        self.columns.extend(['%s_%s' % (i, sd) for i in
                             ['tau_865', 'angstrom', 'solz', 'senz', 'relaz', 'pressure', 'water_vapor', 'l2_flags'] for
                             sd in sds])

        self.outputf = config['root']['outputf']
        self.sub_region = True if str.upper(config['root']['sub_region']) == 'TRUE' else False

        self.GCP_COUNT = 50
        self.window = int(config['root']['window'])

    def setl2(self, l2f):
        # print(l2f)
        d = gdal.Open(l2f)
        subdatasets = d.GetSubDatasets()
        temp1 = subdatasets[0][1].index('x')
        temp2 = subdatasets[0][1].index(']')
        self.RasterX = int(subdatasets[0][1][1:temp1])  # line
        self.RasterY = int(subdatasets[0][1][1 + temp1:temp2])  # pixl

        print(self.RasterX, self.RasterY)

        xDataset = gdal.Open(subdatasets[0][0])  # longitude
        yDataset = gdal.Open(subdatasets[1][0])  # latitude

        gdalSubDataset = gdal.Open(subdatasets[-3][0])

        self.projection = xDataset.GetProjection()
        # print self.RrsIndex[0]
        # self.projection =  'GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],' \
        #                    'AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,' \
        #                    'AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]]'

        # RasterXSize, RasterYSize = gdalSubDataset.RasterXSize, gdalSubDataset.RasterYSize
        # estimate pixel/line step of the geolocation arrays
        pixelStep = math.ceil(float(gdalSubDataset.RasterXSize) /
                              float(xDataset.RasterXSize))
        lineStep = math.ceil(float(gdalSubDataset.RasterYSize) /
                             float(yDataset.RasterYSize))

        gcps = []
        dx, dy, k = .5, .5, 0
        center_lon, center_lat = 0, 0

        # if self.GCP_COUNT >= self.RasterX:
        #     step0 = 1
        # else:
        step0 = int(max(1, self.RasterX) / self.GCP_COUNT)

        # if self.GCP_COUNT >= self.RasterY:
        #     step1 = 1
        # else:
        step1 = int(max(1, self.RasterY) / self.GCP_COUNT)

        # print(step0,step1)

        # step0,step1 = 5,5
        for i0 in range(0, self.RasterX, step0):
            for i1 in range(0, self.RasterY, step1):
                # print(i0,i1)
                # create GCP with X,Y,pixel,line from lat/lon matrices
                lon = xDataset.ReadAsArray(xoff=i1, yoff=self.RasterX - i0 - 1, xsize=1, ysize=1)[0][0]
                lat = yDataset.ReadAsArray(xoff=i1, yoff=self.RasterX - i0 - 1, xsize=1, ysize=1)[0][0]

                if (lon >= -180 and lon <= 180 and lat >= -90 and lat <= 90):
                    gcp = gdal.GCP(float(lon), float(lat), 0, i1 * pixelStep + dx, i0 * lineStep + dy)
                    gcps.append(gcp)
                    center_lon += gcp.GCPX
                    center_lat += gcp.GCPY
                    k += 1

        d.SetGCPs(gcps, osr.SRS_WKT_WGS84_LAT_LONG)
        self.curDataSet = d
        srcWKT = self.projection
        options = ['SRC_SRS=' + srcWKT, 'DST_SRS=' + osr.SRS_WKT_WGS84_LAT_LONG, 'METHOD=GCP_TPS']
        self.transfomer = gdal.Transformer(self.curDataSet, None, options)
        return

    '''
    if lonlat is True, x refers to longitude, y refers to latitude, xt refers to pixel,yt refers to scan
    if lonlat is False, x refers to pixel,y refers to scan, xt,yt refer to longtitude,latitude respectively.
    '''

    def transformPoint(self, x, y, lonlat=True):
        # DstToSrc : 0 or 1, 1 for inverse transformation, 0 for forward transformation.
        DstToSrc = 0
        if lonlat == True:
            DstToSrc = 1
        xt = self.transfomer.TransformPoint(DstToSrc, x, y)[1][0]
        yt = self.transfomer.TransformPoint(DstToSrc, x, y)[1][1]
        return (xt, yt)

    def run(self):
        if self.sub_region:
            self.run_subregion()
        else:
            self.run_entire()

    def run_entire(self):
        productid_station_df = pd.read_csv(self.productid_station_f)
        dirs = os.listdir(self.l2dir)
        dirs.sort()
        data = []
        for index, dir_ in enumerate(dirs):
            if dir_.find("_dark") < 0 or os.path.isdir(os.path.join(self.l2dir, dir_)) == False:
                continue
            product_id = dir_.split("_dark")[0]
            temp_df = productid_station_df[productid_station_df['PRODUCT_ID'] == product_id]
            if temp_df.shape[0] == 0:
                continue

            l2w_f = glob.glob(os.path.join(self.l2dir, dir_, "*L2W.nc"))
            if len(l2w_f) == 0:
                continue
            l2w_f = l2w_f[0]

            self.setl2(l2w_f)
            print(index, len(dirs))
            for index, row in temp_df.iterrows():
                lon, lat = row['Longitude (DD.DDDDD) - five decimal points'], row[
                    'Latitude (DD.DDDDD) - five decimal points']
                # lon,lat  = row['long'],row['lat']
                stationid = row['Station ID']
                # stationid = row['lakepulse_id']

                pixl, line = self.transformPoint(x=lon, y=lat, lonlat=True)
                pixl, line = int(pixl), int(line)
                values_pre = [stationid, l2w_f, "ACOLITE_DarkSpectrum", line, pixl]

                temp_values = self.extract(line, pixl)
                values_pre.extend(temp_values)

                basic_values = row.tolist()
                data.append(basic_values + values_pre)

        if len(data) == 0:
            print("no data ...")
            return
        data = np.array(data)
        print(data.shape, len(self.columns))
        pd.DataFrame(data=data, columns=productid_station_df.columns.tolist() + self.columns).to_csv(self.outputf)

        return

    def run_subregion(self):
        productid_station_df = pd.read_csv(self.productid_station_f)
        data = []
        for i, row in productid_station_df.iterrows():
            dir_ = row['ProductID_StationID']
            abs_dir_ = os.path.join(self.l2dir, dir_)
            if not os.path.exists(abs_dir_):
                continue

            l2w_f = glob.glob(os.path.join(abs_dir_, "*L2W.nc"))

            if len(l2w_f) == 0:
                continue
            l2w_f = l2w_f[0]
            self.setl2(l2w_f)
            # lon, lat = row['Longitude (DD.DDDDD) - five decimal points'], row['Latitude (DD.DDDDD) - five decimal points']
            lon,lat  = row['long'],row['lat']
            stationid = row['Station ID']
            # stationid = row['lakepulse_id']

            pixl, line = self.transformPoint(x=lon, y=lat, lonlat=True)
            pixl, line = int(pixl), int(line)
            values_pre = [stationid, l2w_f, "ACOLITE_DS_SUB_ADJ", line, pixl]

            temp_values = self.extract(line, pixl)
            values_pre.extend(temp_values)

            basic_values = row.tolist()
            data.append(basic_values + values_pre)
        if len(data) == 0:
            print("no data ...")
            return
        data = np.array(data)
        print(data.shape, len(self.columns))
        pd.DataFrame(data=data, columns=productid_station_df.columns.tolist() + self.columns).to_csv(self.outputf)

    def extract(self, line, pixel):
        sds = self.curDataSet.GetSubDatasets()
        values = []
        if line == -1 and pixel == -1:
            for _ in self.bands:
                values.extend([-2, -2, -2, -2, -2])
            return values
        for band in self.bands:
            indexs = [i for i, sd in enumerate(sds) if band[:-1] in sd[0]]
            if len(indexs) == 0:
                # print(band, "no exists")
                values.extend([0, 0, 0, 0, 0])
                continue

            values_v = gdal.Open(sds[indexs[0]][0]).ReadAsArray(xoff=pixel - self.window,
                                                                yoff=self.RasterX - line - self.window - 1,
                                                                xsize=2 * self.window + 1, ysize=2 * self.window + 1)
            scale, offset = 1, 0
            # values_v = ds.value[line - 2:line + 3, pixel - 2:pixel + 3]
            mask = (values_v != 9.96921e+36)
            values_v_m = values_v[mask] * scale + offset
            if values_v_m.shape[0] == 0:
                values_v_m = np.full_like(values_v, -32767)

            values_v_m = values_v_m * scale + offset
            values.append(np.median(values_v_m))
            values.append(np.mean(values_v_m))
            values.append(np.std(values_v_m))
            values.append(np.max(values_v_m))
            values.append(np.min(values_v_m))
        return values


if __name__ == "__main__":
    config = configparser.ConfigParser()
    config.read("config_extractL8L2_ACOLITE_ADJ.ini")
    ws = workspace(config)
    ws.run()
