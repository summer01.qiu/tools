import os,sys
import pandas as pd
import glob

import acolite as ac

def process_single_l8(l1,outputdir,extent,region_name=None):
    settings = {
        'limit': [extent[0], extent[1], extent[2], extent[3]],
        'l2w_parameters': 'Rrs_*',
        'adjacency_correction': True,
        'adjacency_method' : 'ajfilter',
        'dsf_aot_estimate':'fixed',
        'ajfilter_iter':10,
        'dsf_exclude_bands':[8,9],
        'dsf_write_aot_550':True,
        'region_name':region_name,
        'ancillary_data':False,
        'luts':['ACOLITE-LUT-202110-MOD1'],
        'l2w_mask':False

    }
    ac.acolite.acolite_run_test(settings,inputfile=l1,output=outputdir)

if __name__ == '__main__':
    csvf = sys.argv[1]
    input_dir = sys.argv[2]
    output_root_dir = sys.argv[3]
    print(csvf,input_dir,output_root_dir)
    df = pd.read_csv(csvf)
    for i,row in df.iterrows():
        product_id = row['PRODUCT_ID']
        region_name = None
        if 'name' in row:
            region_name = row['name']
        l1s = glob.glob(os.path.join(input_dir,product_id+'*'))
        if len(l1s)==0:
            print("WARN: not found :{}".format(os.path.join(input_dir,product_id+'*')))
            continue
        elif len(l1s)>1:
            print("WARN: more than 1 found :{}".format(os.path.join(input_dir, product_id + '*')))
            continue

        l1f = l1s[0]
        output_dir = os.path.join(output_root_dir, product_id+'_'+str(region_name))

        extent = row[['south','west','north','east']].values
        process_single_l8(l1f, output_dir, extent, region_name=region_name)

